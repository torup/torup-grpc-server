package main

import (
	"context"
	"fmt"
	"log"
	"net"

	tp "torup.local/torup/torProxy"
	"google.golang.org/grpc"
)

func (s *routeTorup) StartTor(ctx context.Context, empty *tp.Empty) (*tp.Bool, error) {
	return tp.StartTor()
}

func (s *routeTorup) StopTor(ctx context.Context, empty *tp.Empty) (*tp.Bool, error) {
	return tp.StopTor()
}

func (s *routeTorup) MakeHTTPRequest(ctx context.Context, request *tp.HTTPRequest) (*tp.HTTPResponse, error) {
	return tp.MakeHTTPRequest(request.Url, request.EnableProxy)
}

type routeTorup struct {
	tp.UnimplementedTorupServiceServer
}

func newServer() *routeTorup {
	s := &routeTorup{}
	return s
}

func main() {

	log.Printf("[TorUp] GRPC Server launched!")

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 9000))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	} else {
		log.Printf("Listening on port 9000...")
	}

	grpcServer := grpc.NewServer()

	tp.RegisterTorupServiceServer(grpcServer, newServer())

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}