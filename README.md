<h1 align="center">Welcome to torup-grpc-server 👋</h1>
<p>
</p>

> Backend gRPC (Golang) pour TorUp-app 

## Install

```sh
protoc --go_out=. --go-grpc_out=. *.proto
```

## Usage

```sh
go run server.go
```

## build
```sh
env GOOS=windows GOARCH=amd64 go build -o build/torup-grpc-server.exe
env GOOS=darwin GOARCH=amd64 go build -o build/torup-grpc-server
```

## Author

👤 **Bigeyes - bigeyeswashere@protonmail.com**


## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_