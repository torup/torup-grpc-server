module torup.local/torup/main

go 1.14

require (
	github.com/mitchellh/go-ps v1.0.0 // indirect
	google.golang.org/grpc v1.39.0
	torup.local/torup/torProxy v0.0.0-00010101000000-000000000000
)

replace torup.local/torup/torProxy => ./torProxy
