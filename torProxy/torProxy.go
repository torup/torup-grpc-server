package torProxy

import (
	"os/exec"
	"log"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"fmt"

	ps "github.com/mitchellh/go-ps"
	"golang.org/x/net/proxy"
)

const (
	PROXY_ADDR = "127.0.0.1:9050"
	URL        = "http://skunksworkedp2cg.onion/sites.html"
)

var torProcess *exec.Cmd
var torHost = "127.0.0.1"
var torPort = 9050

func FindProcess(key string) (int, string, error) {
	pname := ""
	pid := 0
	err := errors.New("not found")
	ps, _ := ps.Processes()
	for i, _ := range ps {
		if ps[i].Executable() == key {
			pid = ps[i].Pid()
			pname = ps[i].Executable()
			err = nil
			break
		}
	}
	return pid, pname, err
}

func GetProcessPid(processName string) (int, bool) {
	if pid, _, err := FindProcess("tor"); err == nil {
		return pid, true
	}
	return 0, false
}

func WaitingForTorProcess() {
	done := make(chan struct{})
	go (func () {
		torProcess.Wait()
		close(done)
	})()
}

func StartTor() (*Bool, error) {
	if _, isUp := GetProcessPid("tor"); !isUp {
		torProcess = exec.Command("tor")
		if err := torProcess.Start(); err != nil {
			log.Fatal(err)
			return &Bool{Result: false, Details: err.Error()}, nil
		}
		WaitingForTorProcess()
	} else {
		return &Bool{Result: false, Details: "Un processus Tor est déjà lancé"}, nil
	}
	return &Bool{Result: true, Details: "Le processus Tor est lancé"}, nil
}

func StopTor() (*Bool, error) {
	if _, isUp := GetProcessPid("tor"); isUp {
		if err := torProcess.Process.Kill(); err != nil {
			log.Fatal("failed to kill process: ", err)
			return &Bool{Result: false, Details: err.Error()}, nil
		}
		torProcess = nil
		return &Bool{Result: true, Details: "Le processus Tor est stoppé"}, nil
	}
	return &Bool{Result: false, Details: "Aucun processus Tor à stopper"}, nil
}

func MakeHTTPRequest(url string, proxyEnabled bool) (*HTTPResponse, error) {
	PROXY_ADDR := "127.0.0.1:9050"
	URL := url

	httpTransport := &http.Transport{}
	httpClient := &http.Client{Transport: httpTransport}

	if proxyEnabled {
		dialer, err := proxy.SOCKS5("tcp", PROXY_ADDR, nil, proxy.Direct)
		if err != nil {
			fmt.Fprintln(os.Stderr, "can't connect to the proxy:", err)
			os.Exit(1)
		}
		httpTransport.Dial = dialer.Dial
	}

	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		fmt.Fprintln(os.Stderr, "can't create request:", err)
		os.Exit(2)
	}
	resp, err := httpClient.Do(req)
	if err != nil {
		fmt.Fprintln(os.Stderr, "can't GET page:", err)
		os.Exit(3)
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Fprintln(os.Stderr, "error reading body:", err)
		os.Exit(4)
	}
	return &HTTPResponse{Data: string(b)}, nil
}